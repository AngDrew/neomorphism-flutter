import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:neomorphism_project/src/resources/my_color.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: TestClass(),
    );
  }
}

class TestClass extends StatefulWidget {
  @override
  _TestClassState createState() => _TestClassState();
}

class _TestClassState extends State<TestClass> {
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: SafeArea(
        child: Container(
          width: size.width,
          height: size.height,
          alignment: Alignment.center,
          child: Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              boxShadow: <BoxShadow>[
                BoxShadow(
                  offset: const Offset(-3, -3),
                  color: Colors.white,
                  blurRadius: 3,
                  spreadRadius: 1,
                ),
                BoxShadow(
                  offset: const Offset(3, 3),
                  color: Colors.black.withOpacity(0.2),
                  blurRadius: 3,
                  spreadRadius: 1,
                ),
              ],
              // gradient: LinearGradient(
              //   begin: Alignment.topLeft,
              //   end: Alignment.bottomRight,
              //   colors: <Color>[
              //     Colors.grey[50],
              //     Colors.grey[100],
              //     Colors.grey[200],
              //     Colors.grey[300],
              //   ],
              // ),
              shape: BoxShape.circle,
            ),
          ),
        ),
      ),
    );
  }
}
